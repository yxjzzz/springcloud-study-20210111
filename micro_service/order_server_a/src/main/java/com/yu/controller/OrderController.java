package com.yu.controller;

import com.yu.entity.Order;
import com.yu.service.OrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Order)表控制层
 *
 * @author yuxiujie
 * @since 2021-01-11 17:08:26
 */
@RestController
@RequestMapping("order")
public class OrderController {
    /**
     * 服务对象
     */
    @Resource
    private OrderService orderService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Order selectOne(Integer id) {
        return this.orderService.queryById(id);
    }


    /**
     * 通过实体作为筛选条件查询
     *
     * @param
     * @return 对象列表
     */
    @GetMapping("queryAll")
   public List<Order> queryAll(){
       return this.orderService.queryAll(null);
   }
}