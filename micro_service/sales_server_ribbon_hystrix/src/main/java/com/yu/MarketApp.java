package com.yu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 10:59
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.yu.dao")
@EnableTransactionManagement
@EnableHystrix
public class MarketApp {
    public static void main(String[] args) {
        SpringApplication.run(MarketApp.class,args);
    }
}
