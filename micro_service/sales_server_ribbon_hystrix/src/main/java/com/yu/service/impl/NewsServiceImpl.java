package com.yu.service.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.yu.dao.NewsDao;
import com.yu.entity.News;
import com.yu.service.NewsService;
import com.yu.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (News)表服务实现类
 *
 * @author yuxiujie
 * @since 2021-01-14 11:04:45
 */
@Service("newsService")
public class NewsServiceImpl implements NewsService {
    @Resource
    private NewsDao newsDao;

    @Autowired
    private OrderService orderService;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public News queryById(Integer id) {
        return this.newsDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<News> queryAllByLimit(int offset, int limit) {
        return this.newsDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param news 实例对象
     * @return 实例对象
     */
    @Override
    public News insert(News news) {
        this.newsDao.insert(news);
        return news;
    }

    /**
     * 修改数据
     *
     * @param news 实例对象
     * @return 实例对象
     */
    @Override
    public News update(News news) {
        this.newsDao.update(news);
        return this.queryById(news.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.newsDao.deleteById(id) > 0;
    }

    /**
     * 查询多条数据
     *
     * @param news News
     * @return 对象列表
     */
    @Override
    @HystrixCommand(fallbackMethod = "queryAllFallBackMethod")
    public Map<String, Object> queryAll(News news) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("newsList",this.newsDao.queryAll(news));
        resultMap.put("orderList",this.orderService.queryAll());
        return resultMap;
    }

    public Map<String, Object> queryAllFallBackMethod(News news) {
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("errorNum","44444");
        resultMap.put("errorInfo","请求错误");
        return resultMap;
    }
}