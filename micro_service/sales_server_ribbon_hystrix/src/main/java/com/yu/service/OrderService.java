package com.yu.service;

import com.yu.entity.Order;

import java.util.List;

/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 11:13
 */

public interface OrderService {

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
     Order selectOne(Integer id);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param
     * @return 对象列表
     */
    List<Order> queryAll();
}
