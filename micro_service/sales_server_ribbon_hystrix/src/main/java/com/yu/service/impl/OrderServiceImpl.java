package com.yu.service.impl;

import com.yu.entity.Order;
import com.yu.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 11:15
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Order selectOne(Integer id) {
        return restTemplate.getForObject("http://orderService/order/selectOne",Order.class);
    }

    @Override
    public List<Order> queryAll() {
        return restTemplate.getForObject("http://orderService/order/queryAll",List.class);
    }
}
