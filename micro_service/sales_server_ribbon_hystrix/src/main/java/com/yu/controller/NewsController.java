package com.yu.controller;

import com.yu.entity.News;
import com.yu.service.NewsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * (News)表控制层
 *
 * @author yuxiujie
 * @since 2021-01-14 11:04:45
 */
@RestController
@RequestMapping("news")
public class NewsController {
    /**
     * 服务对象
     */
    @Resource
    private NewsService newsService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public News selectOne(Integer id) {
        return this.newsService.queryById(id);
    }


    /**
     * 通过实体作为筛选条件查询
     *
     * @return 对象列表
     */
    @GetMapping("queryAll")
    public Map<String, Object> queryAll() {
        return this.newsService.queryAll(null);
    }

}