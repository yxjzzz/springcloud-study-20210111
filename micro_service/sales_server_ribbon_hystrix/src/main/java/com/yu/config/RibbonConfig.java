package com.yu.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 11:10
 */
@Configuration
public class RibbonConfig {

    /**
     * 功能描述:实例化RestTemplate(ribbon借用这个类实现远程调用)
     * @return org.springframework.web.client.RestTemplate
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public IRule irule(){
        return new RoundRobinRule();
    }
}
