package com.yu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author Xiujie Yu
 * @Date 2021-01-13
 * @Time 11:15
 */
@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
public class ProviderHystrix {
    public static void main(String[] args) {
        SpringApplication.run(ProviderHystrix.class,args);
    }
}
