package com.yu.controller;

import com.yu.service.PaymentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Xiujie Yu
 * @Date 2021-01-13
 * @Time 11:26
 */
@RestController
public class PaymentControlelr {
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String ServerPort;

    @GetMapping("/payment/hystrix/ok")
    public String paymentInfo_ok(){
        String s = paymentService.paymentInfo_ok();
        System.out.println(s);
        return s;
    }
    @GetMapping("/payment/hystrix/timeOut")
    public String paymentInfo_TimeOut(){
        String s = paymentService.paymentInfo_TimeOut();
        System.out.println(s);
        return s;
    }
}
