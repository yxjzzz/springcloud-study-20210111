package com.yu.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author Xiujie Yu
 * @Date 2021-01-13
 * @Time 11:20
 */
@Service
public class PaymentService {

    public String paymentInfo_ok() {
        return "线程池" + Thread.currentThread().getName() + "  paymentInfo_ok " + "\t" + "hah";
    }


    @HystrixCommand(fallbackMethod = "paymentInfo_TimeOutHandler", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "5000")
    })
    public String paymentInfo_TimeOut() {
        int timeNumber = 3;
        try {
            TimeUnit.SECONDS.sleep(timeNumber);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "线程池" + Thread.currentThread().getName() + "  paymentInfo_TimeOut " + "\t" + "耗时" + timeNumber + "秒";
    }

    public String paymentInfo_TimeOutHandler() {
        return "线程池" + Thread.currentThread().getName() + "  paymentInfo_TimeOutHandler " + "\t" + "❥(^_-)";
    }
}
