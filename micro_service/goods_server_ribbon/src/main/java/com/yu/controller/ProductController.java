package com.yu.controller;

import com.yu.entity.Product;
import com.yu.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * (Product)表控制层
 *
 * @author yuxiujie
 * @since 2021-01-12 09:52:14
 */
@RestController
@RequestMapping("product")
public class ProductController {
    /**
     * 服务对象
     */
    @Resource
    private ProductService productService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Product selectOne(Long id) {
        return this.productService.queryById(id);
    }


    /**
     * 通过实体作为筛选条件查询
     *
     * @param product 实例对象
     * @return 对象列表
     */
    @GetMapping("queryAll")
    public Map<String, List> queryAll(Product product) {
        return this.productService.queryAll(null);
    }

}