package com.yu.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Xiujie Yu
 * @Date 2021-01-12
 * @Time 10:18
 */
@Configuration
public class MyRule {
    @Bean
    public IRule iRule(){
        return new RoundRobinRule();
    }
}
