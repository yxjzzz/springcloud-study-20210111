package com.yu.service.impl;

import com.yu.dao.ProductDao;
import com.yu.entity.Product;
import com.yu.service.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * (Product)表服务实现类
 *
 * @author yuxiujie
 * @since 2021-01-12 09:52:14
 */
@Service("productService")
public class ProductServiceImpl implements ProductService {

    private final RestTemplate restTemplate;

    @Resource
    private ProductDao productDao;

    public ProductServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Product queryById(Long id) {
        return this.productDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Product> queryAllByLimit(int offset, int limit) {
        return this.productDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param product 实例对象
     * @return 实例对象
     */
    @Override
    public Product insert(Product product) {
        this.productDao.insert(product);
        return product;
    }

    /**
     * 修改数据
     *
     * @param product 实例对象
     * @return 实例对象
     */
    @Override
    public Product update(Product product) {
        this.productDao.update(product);
        return this.queryById(product.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.productDao.deleteById(id) > 0;
    }

    /**
     * 查询多条数据
     *
     * @param product Product
     * @return 对象列表
     */
    @Override
    public Map<String, List> queryAll(Product product) {
        Map<String, List> resultMap = new HashMap<>();
        resultMap.put("ProductList", this.productDao.queryAll(product));
        resultMap.put("OrderList", this.queryOrderList(product));
        return resultMap;
    }

    public List<Product> queryOrderList(Product product) {
        return restTemplate.getForObject("http://orderService/order/queryAll",List.class);
    }
}