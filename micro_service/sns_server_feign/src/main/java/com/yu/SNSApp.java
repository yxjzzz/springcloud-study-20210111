package com.yu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Xiujie Yu
 * @Date 2021-01-12
 * @Time 21:00
 */
@SpringBootApplication
@EnableFeignClients
@MapperScan("com.yu.dao")
@EnableTransactionManagement
public class SNSApp {
    public static void main(String[] args) {
        SpringApplication.run(SNSApp.class,args);
    }
}
