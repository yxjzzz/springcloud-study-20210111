package com.yu.controller;

import com.yu.entity.Order;
import com.yu.service.SnsFeignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Comment)表控制层
 *
 * @author yuxiujie
 * @since 2021-01-12 21:41:38
 */
@RestController
public class CommentController {


    @Resource
    private SnsFeignService snsFeignService;

    @GetMapping("/feign/queryAll")
    public List<Order> queryAllOrderList(){
        return snsFeignService.queryAll();
    }
}