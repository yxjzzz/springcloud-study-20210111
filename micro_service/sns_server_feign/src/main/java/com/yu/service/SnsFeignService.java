package com.yu.service;

import com.yu.entity.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author Xiujie Yu
 * @Date 2021-01-12
 * @Time 22:09
 */
@Component
@FeignClient(value = "orderService")
public interface SnsFeignService {
    @GetMapping("/order/queryAll")
    public List<Order> queryAll();
}
