package com.yu.controller;

import com.yu.entity.Member;
import com.yu.service.MemberService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 会员表(Member)表控制层
 *
 * @author yuxiujie
 * @since 2021-01-14 11:45:33
 */
@RestController
@RequestMapping("member")
public class MemberController {
    /**
     * 服务对象
     */
    @Resource
    private MemberService memberService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Member selectOne(Long id) {
        return this.memberService.queryById(id);
    }


    /**
     * 通过实体作为筛选条件查询
     *
     * @param member 实例对象
     * @return 对象列表
     */
    @GetMapping("queryAll")
    public Map<String,Object> queryAll(Member member) {
        return this.memberService.queryAll(member);
    }

}