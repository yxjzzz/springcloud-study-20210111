package com.yu.service;

import com.yu.service.impl.NewsServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 11:47
 */
@FeignClient(name = "salesService",fallback = NewsServiceFallBack.class)
public interface NewsService {

    /**
     * 查询全部
     * @return 对象列表
     */
    @GetMapping("/news/queryAll")
    Map<String, Object> queryAll();
}
