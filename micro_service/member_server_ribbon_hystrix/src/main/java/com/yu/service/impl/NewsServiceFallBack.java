package com.yu.service.impl;

import com.yu.service.NewsService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 14:14
 */
@Component
public class NewsServiceFallBack implements NewsService {

    @Override
    public Map<String, Object> queryAll() {
        Map<String, Object> map = new HashMap<>();
        map.put("errorCode","4444");
        map.put("errorInfo","远程服务调用失败，请联系管理员！！");
        return map;
    }
}
