package com.yu.service.impl;

import com.yu.dao.MemberDao;
import com.yu.entity.Member;
import com.yu.service.MemberService;
import com.yu.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会员表(Member)表服务实现类
 *
 * @author yuxiujie
 * @since 2021-01-14 11:45:33
 */
@Service("memberService")
public class MemberServiceImpl implements MemberService {
    @Resource
    private MemberDao memberDao;

    @Autowired
    private NewsService newsService;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Member queryById(Long id) {
        return this.memberDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Member> queryAllByLimit(int offset, int limit) {
        return this.memberDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param member 实例对象
     * @return 实例对象
     */
    @Override
    public Member insert(Member member) {
        this.memberDao.insert(member);
        return member;
    }

    /**
     * 修改数据
     *
     * @param member 实例对象
     * @return 实例对象
     */
    @Override
    public Member update(Member member) {
        this.memberDao.update(member);
        return this.queryById(member.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.memberDao.deleteById(id) > 0;
    }

    /**
     * 查询多条数据
     *
     * @param member Member
     * @return 对象列表
     */
    @Override
    public Map<String,Object> queryAll(Member member) {
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("memberList",this.memberDao.queryAll(member));
        resultMap.put("newsAndOrderList",this.newsService.queryAll());
        return resultMap;
    }
}