package com.yu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 11:43
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.yu.dao")
@EnableTransactionManagement
@EnableFeignClients
public class MemberApp {
    public static void main(String[] args) {
        SpringApplication.run(MemberApp.class,args);
    }
}
