package com.yu.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Xiujie Yu
 * @Date 2021-01-13
 * @Time 14:51
 */
@Component
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT")
public interface PaymentHystrixService {


    @GetMapping("/payment/hystrix/ok")
    public String paymentInfo_ok();

    @GetMapping("/payment/hystrix/timeOut")
    public String paymentInfo_TimeOut();
}
