package com.yu.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.yu.service.PaymentHystrixService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Xiujie Yu
 * @Date 2021-01-13
 * @Time 14:53
 */
@RestController
@DefaultProperties(defaultFallback = "payment_Global_FallBackMethod")
public class OrderHystrixController {
    @Resource
    private PaymentHystrixService paymentHystrixService;

    @GetMapping("/payment/hystrix/ok")
    public String paymentInfo_ok(){
        return paymentHystrixService.paymentInfo_ok();
    }

    @GetMapping("/payment/hystrix/timeOut")
    /*@HystrixCommand(fallbackMethod = "paymentTimeOutHandlerFallBackMethod", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "1500")
    })*/
    @HystrixCommand
    public String paymentInfo_TimeOut(){
        return paymentHystrixService.paymentInfo_TimeOut();
    }

    public String paymentTimeOutHandlerFallBackMethod(){
        return "我是消费者80，对方支付系统繁忙，请10秒后再试或检查自己";
    }

    public String payment_Global_FallBackMethod(){
        return "Global异常处理信息，请稍后再试。o(╥﹏╥)o";
    }
}
