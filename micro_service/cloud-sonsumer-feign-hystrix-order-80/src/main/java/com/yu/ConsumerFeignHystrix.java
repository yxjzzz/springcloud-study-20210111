package com.yu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author Xiujie Yu
 * @Date 2021-01-13
 * @Time 14:49
 */
@SpringBootApplication
@EnableFeignClients
@EnableHystrix
public class ConsumerFeignHystrix {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerFeignHystrix.class,args);
    }
}
