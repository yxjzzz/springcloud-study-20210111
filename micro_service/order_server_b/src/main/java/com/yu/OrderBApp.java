package com.yu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author Xiujie Yu
 * @Date 2021-01-11
 * @Time 16:50
 */
@SpringBootApplication
@EnableEurekaClient //当注册中心为eureka时用这个
//@EnableDiscoveryClient 任何注册中心都可以使用这个
@MapperScan("com.yu.dao")
public class OrderBApp {
    public static void main(String[] args){
        SpringApplication.run(OrderBApp.class,args);
    }
}
