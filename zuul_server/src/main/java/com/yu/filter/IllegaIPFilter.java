package com.yu.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * @author Xiujie Yu
 * @Date 2021-01-14
 * @Time 15:19
 * 过滤非法IP
 */
@Component
public class IllegaIPFilter extends ZuulFilter {

    @Value("${illegal_ip}")
    private List<String> illegalIP;

    @Override
    public String filterType() {
        /*
        过滤器使用类型
            pre   在调用后台微服务中业务方法之前  调用该过滤器
            route 在调用后台微服务中业务方法时    调用该过滤器
            post  在调用后台微服务中业务方法之后  调用该过滤器
            error  在调用后台微服务中业务方法出现异常时  调用该过滤器
         */
        return "pre";
    }

    @Override
    public int filterOrder() {
        //多个filter时，当前filter执行的优先级  返回值越小，优先级越高
        return 5;
    }

    @Override
    public boolean shouldFilter() {
        //是否当前的过滤器生效  true 生效 false 失效
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //具体过滤器业务代码
        System.out.println("----进入了IP过滤器----");
        //使用zuul中提供的请求上下文对象
        RequestContext currentContext = RequestContext.getCurrentContext();
        //获取response
        HttpServletResponse response = currentContext.getResponse();
        //获取request
        HttpServletRequest request = currentContext.getRequest();
        //获取请求用户的IP地址
        String remoteAddr = request.getRemoteAddr();
        System.out.println("请求Ip为："+remoteAddr);
        //判断IP地址是否合法
        if (illegalIP.contains(remoteAddr)){
            //阻止程序继续运行
            currentContext.setSendZuulResponse(false);

            try {
                response.sendError(HttpStatus.SC_FORBIDDEN,"你的IP非法，暂不能访问");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return null;
    }
}
