package com.yu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * (News)实体类
 *
 * @author yuxiujie
 * @since 2021-01-14 11:04:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class News implements Serializable {
    private static final long serialVersionUID = 383246810719972788L;

    private Integer id;

    private String title;

    private String content;

    private Date addtime;

    private Integer clicknum;

}