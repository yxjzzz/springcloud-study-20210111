package com.yu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Product)实体类
 *
 * @author yuxiujie
 * @since 2021-01-12 09:52:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product implements Serializable {
    private static final long serialVersionUID = 322719339111819625L;

    private Long id;
    /**
     * 店铺ID
     */
    private Long shopId;
    /**
     * 品牌ID
     */
    private Long brandId;
    /**
     * 产品类别ID
     */
    private Long categoryId;

    private String name;

    private String pic;

}