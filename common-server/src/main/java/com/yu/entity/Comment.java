package com.yu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Comment)实体类
 *
 * @author yuxiujie
 * @since 2021-01-12 21:41:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment implements Serializable {
    private static final long serialVersionUID = -39382672151955552L;

    private Long id;

    private Long shopId;
    /**
     * 订单ID
     */
    private Long orderId;
    /**
     * 订单为单一商品时，该字段有值
     */
    private Long productId;

    private String memberNickName;

    private String productName;
    /**
     * 评价星数：0->5
     */
    private Integer star;

}