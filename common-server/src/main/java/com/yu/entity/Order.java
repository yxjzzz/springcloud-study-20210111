package com.yu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Order)实体类
 *
 * @author yuxiujie
 * @since 2021-01-11 17:08:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable {
    private static final long serialVersionUID = -25950021764432298L;
    /**
     * 订单id
     */
    private Integer id;

    private Integer memberId;

    private Integer shopId;
    /**
     * 拼团活动ID
     */
    private Integer groupPromotionId;
    /**
     * 促销活动ID
     */
    private Integer couponId;
    /**
     * 订单编号
     */
    private String orderSn;
    /**
     * 用户帐号
     */
    private String memberUsername;
    /**
     * 订单总金额
     */
    private Double totalAmount;

}