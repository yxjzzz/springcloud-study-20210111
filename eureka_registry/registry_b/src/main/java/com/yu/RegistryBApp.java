package com.yu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Xiujie Yu
 * @Date 2021-01-11
 * @Time 16:12
 */
@SpringBootApplication
@EnableEurekaServer
public class RegistryBApp {
    public static void main(String[] args) {
        SpringApplication.run(RegistryBApp.class,args);
    }
}
