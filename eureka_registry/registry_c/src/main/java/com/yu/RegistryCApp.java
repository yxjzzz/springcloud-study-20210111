package com.yu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Xiujie Yu
 * @Date 2021-01-11
 * @Time 16:13
 */
@SpringBootApplication
@EnableEurekaServer
public class RegistryCApp {
    public static void main(String[] args) {
        SpringApplication.run(RegistryCApp.class,args);
    }
}
